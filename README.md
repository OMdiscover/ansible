# Descriptif du projet

Ce projet a pour but de déployer l'application ICD Meteo ainsi que les outils de monitoring Prométheus, Zabbix et Grafana, sur un cluster kubernetes d'une infrastructure générée via le projet terraform.
- projet terraform : https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/terraform.git

> A noter qu'il faut au préalable avoir créé l'infrastructure via Terraform (lien du GitLab ci-dessus).

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/ansible.git
```

# Création d'un environnement virtuel

```sh
cd ansible
python3 -m venv ./venv/ansible
source ./venv/ansible/bin/activate
pip install ansible
deactivate
```
# Création des fichiers hosts.ini, .ssh et ssh.cfg

Le projet terraform a généré les fichiers "hosts.ini", ".ssh" et "ssh.cfg".
Copiez les dans le répertoire de travail.
Ajouter un chmod 600 aux clés.

```sh
cp ../terraform/hosts.ini ansible/
cp ../terraform/.ssh ansible/
cp ../terraform/ssh.cfg ansible/
chmod 600 .ssh/*
```

# Création du fichier de variable pour le rôle zabbix-agent

```sh
export PRIV_IP_ZAB=$(cat ../terraform/ip_files/private_ips.txt | grep zabbix-server | awk '{print $3}') && envsubst '$PRIV_IP_ZAB' < roles/zabbix-agent/defaults/main.yml.template > roles/zabbix-agent/defaults/main.yml
```

# Vérifier que les instances sont joignables

```sh
source ./venv/ansible/bin/activate
ANSIBLE_HOST_KEY_CHECKING=False ansible -i hosts.ini all -m ping
```

# Déploiement de l'application

Pour déployer l'application et les outils de monitoring, lancer la commande suivante :

```sh
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts.ini deploy.yml
deactivate
```

> Si une erreur survient lors de l'étape de reboot ou de déploiement des manifestes, relancer le playbook.

# Accéder à l'application

Pour accéder à l'application et aux outils de monitoring, rendez-vous aux adresses suivantes :

> Les IP se trouvent dans le fichier "hosts.ini".

- ICD Meteo :
```sh
http://<MASTER_PUB_IP>
```

- Prométheus :
```sh
http://<MASTER_PUB_IP>:30000
```

- Zabbix :
```sh
http://<ZABBIX_SERVER_PUB_IP>/zabbix
```

- Grafana :
```sh
http://<MASTER_PUB_IP>:32000
```
